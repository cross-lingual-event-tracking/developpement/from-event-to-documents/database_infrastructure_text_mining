#!/bin/bash

# $1 : boolean to indicate if we want generate the noise or the no noise corpus
# $2: Path to read the pickle data
# $3: Path to write the json data

./concate_pickle.py --pickle-repository $2 --save-to $3
if $1;
then
    ./keep_only_noised_documents.py --concat-pickle-repository $3 --save-to $3_no_noise --annotated_events annotated_events_clusters.csv
fi