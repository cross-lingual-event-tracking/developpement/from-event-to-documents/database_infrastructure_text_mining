#!/usr/bin/env python3
#
# Copyright (C) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>,
# Copyright (C) 2022 − Present  Thomas Blot <thomas.blot@etudiant.univ-lr.fr>
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
from xml.dom.minidom import Document

import pandas as pd
import argparse


def main():
    parser = argparse.ArgumentParser(description="Keep only noised documents.")
    parser.add_argument(
        "--concat-pickle-repository",
        type=str,
        required=True,
        help="Path where are stored pickle data",
    )
    parser.add_argument(
        "--save-to",
        type=str,
        required=True,
        help="Path where'll be store pickle events",
    )
    parser.add_argument(
        "--annotated_events",
        type=str,
        required=True,
        help="Path where is store annotated_events",
    )
    args = parser.parse_args()
    if not os.path.exists(f"{args.save_to}"):
        os.makedirs(f"{args.save_to}")
    annotated_events = pd.read_csv(args.annotated_events)
    list_of_cluster_annotated = annotated_events.cluster_id.astype(int).to_list()

    for file in os.listdir(args.concat_pickle_repository):
        documents = pd.read_pickle(f"{args.concat_pickle_repository}/{file}")
        reduce_with_annotated_document_only = documents[
            documents.cluster.astype(int).isin(list_of_cluster_annotated)
        ]
        reduce_with_annotated_document_only.to_pickle(f"{args.save_to}/{file}")


if __name__ == "__main__":
    main()
