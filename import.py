#!/usr/bin/env python3
#
# Copyright (C) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>,
# Copyright (C) 2022 − Present  Thomas Blot <thomas.blot@etudiant.univ-lr.fr>
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import abc
import argparse
import json
import os
from typing import Dict, Any

from tqdm import tqdm

import urllib3
from elasticsearch import Elasticsearch, helpers

# We do disable certificate verification, so it’s clearer to disable warnings
urllib3.disable_warnings()


class Database(abc.ABC):
    def import_data(self, data: dict, speed: int, drop_data: bool):
        pass


class ElasticSearch(Database):
    def __init__(self, host: str, password: str, port: int, user: str):
        self.__connection = Elasticsearch(f"https://{host}:{port}", basic_auth=(user, password), verify_certs=False)

    def import_data(
        self, index_data_mapping: Dict[str, Any], batch_size: int, drop_existing_index_with_same_name: bool
    ):
        if not self.__connection.ping():
            raise ValueError("Connection failed")
        else:
            for elastic_index_name, documents in index_data_mapping.items():
                if drop_existing_index_with_same_name:
                    self.__drop_index(elastic_index_name)
                for batch_index in tqdm(
                    range(0, len(documents), batch_size),
                    total=round(len(documents) / batch_size),
                    desc=f"Import ’{elastic_index_name}’",
                ):
                    documents_in_batch = documents[batch_index : batch_index + batch_size]
                    helpers.bulk(self.__connection, documents_in_batch, index=elastic_index_name)

    def __drop_index(self, index: str):
        try:
            self.__connection.indices.delete(index)
        except (ValueError, KeyError, TypeError):
            pass


class ImportData(abc.ABC):
    def import_data(self, file: str, repository: str):
        pass


class ImportDataFromJson(ImportData):
    def import_data(self, file: str, repository: str):
        if file.endswith(".json"):
            with open(f"{repository}/{file}", mode="r", encoding="utf-8") as input_file_reader:
                data = json.load(input_file_reader)
            return data


def main():
    parser = argparse.ArgumentParser(description="Upload list of documents to an ElasticSearch instance.")

    parser.add_argument(
        "--host",
        type=str,
        required=False,
        help="ElasticSearch instance IP Address to connect to.",
        default="localhost",
    )
    parser.add_argument(
        "--port",
        type=int,
        required=False,
        help="Port used to connect to ElasticSearch REST API.",
        default=9200,
    )
    parser.add_argument(
        "--user",
        type=str,
        required=False,
        help="ElasticSearch user login, used to access the REST API.",
        default="elastic",
    )
    parser.add_argument(
        "--password",
        type=str,
        required=False,
        help="ElasticSearch user password, used to access the REST API.",
    )
    parser.add_argument(
        "--data-repository",
        type=str,
        required=True,
        help="Path where are stored the data files that will be loaded into ElasticSearch indexes.",
    )
    parser.add_argument(
        "--data-type",
        type=str,
        required=False,
        choices=["json"],
        help="Type of data to use as input to upload to the index.",
        default="json",
    )
    parser.add_argument(
        "--clear-index",
        type=bool,
        required=False,
        help="Clear index before importing data. Removes the content of any existing index with the same name.",
        default=False,
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        required=False,
        help="Batch size used to handle documents. The higher it is, the more it consumes computer resources, but the faster.",
        default=1000,
    )

    args = parser.parse_args()
    if args.password:
        database = ElasticSearch(
            args.host,
            args.password,
            args.port,
            args.user,
        )
    else:
        raise ValueError("Missing ElasticSearch database password.")

    data = dict()
    if args.data_type.lower() == "json":
        import_data = ImportDataFromJson()
        for file in os.listdir(args.data_repository):
            data.update({file.split(".")[0]: (import_data.import_data(file, args.data_repository))})


    database.import_data(data, args.batch_size, args.clear_index)


if __name__ == "__main__":
    main()
