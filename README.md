# `database_infrastructure_text_mining`

This project aims at building a text mining infrastructure based on [ElasticSearch](https://www.elastic.co/), powered by [Lucene](https://lucene.apache.org/) to perform text lookups. It’s purpose is to index news article documents in order to search for documents associated to specific descriptions.

## Start the ElasticSearch instance

The `start_elasticsearch_pod.sh` Shell script is used to start the [Podman](https://podman.io/) Pod that provides the ElasticSearch service. Run it and ElasticSearch REST API will be available on `127.0.0.1:9200` (the standard EL API port).

The default ElasticSearch user is `elastic`, as it the default password. The first argument given to the shell script will be used as the Elastic password.

## Convert corpus of documents into JSON

ElasticSearch stores documents in its index as JSON documents. We handle [`document_tracking_resources.corpus.Corpus`](https://gitlab.univ-lr.fr/cross-lingual-event-tracking/developpement/from-documents-to-events/documents_tracking_resources) based corpus of articles stored in `.pickle` files. In order to convert such datasets into a unique `.json` file that can be loaded into ElasticSearch, you can use the `convert_document_tracking_resources_corpus_to_json.py` script.

## Import data

Once ElasticSearch is up and running, the `import.py` script take previously exported `.json` corpus file and uploads it to the ElasticSearch index. Call help on this script to see the available parameters you can use.

## Requirement

You need to put here `event_registry_annotated_events_clusters.csv` that contains the following, for instance:

```csv
,cluster_id,event_id,min_date,max_date,headlines_eng,headlines_spa,headlines_deu,wikidata_event_id,real_event,specific_event_on_wikipedia,comment
```
