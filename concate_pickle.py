#!/usr/bin/env python3
#
# Copyright (C) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>,
# Copyright (C) 2022 − Present  Thomas Blot <thomas.blot@etudiant.univ-lr.fr>
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
from xml.dom.minidom import Document

import pandas as pd
import argparse


def main():
    parser = argparse.ArgumentParser(description="Concat pickle file.")
    parser.add_argument(
        "--pickle-repository",
        type=str,
        required=True,
        help="Path where are stored pickle data",
    )
    parser.add_argument(
        "--save-to",
        type=str,
        required=True,
        help="Path where'll be store pickle events",
    )

    args = parser.parse_args()
    if not os.path.exists(f"{args.save_to}"):
        os.makedirs(f"{args.save_to}")

    for directory in os.listdir(args.pickle_repository):
        pickles = []
        for file in os.listdir(f"{args.pickle_repository}/{directory}"):
            pickles.append(
                pd.read_pickle(f"{args.pickle_repository}/{directory}/{file}")
            )
        df = pd.concat(pickles)
        df.to_pickle(f"{args.save_to}/{directory}.pickle")


if __name__ == "__main__":
    main()
