#!/bin/bash

declare -r ELASTIC_PASSWORD="${1:-elastic}"

docker stop es01
docker rm es01
docker run --name es01 -d -e ELASTIC_PASSWORD="${ELASTIC_PASSWORD}" --net elastic -p 9200:9200 -it docker.elastic.co/elasticsearch/elasticsearch:8.1.1 

