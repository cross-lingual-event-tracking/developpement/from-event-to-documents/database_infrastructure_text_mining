#!/usr/bin/env python3
#
# Copyright (C) 2022 − Present Guillaume Bernard <contact@guillaume-bernard.fr>,
# Copyright (C) 2022 − Present  Thomas Blot <thomas.blot@etudiant.univ-lr.fr>
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import argparse
import json
import os
import spacy
from spacy import displacy
from pandas import DataFrame, read_pickle

language_processor = spacy.load("xx_ent_wiki_sm")
language_processor.add_pipe("sentencizer")


def _remove_ocr_markup_from_raw_text(input_text: str):
    """
    The noise consists of characters recognized by the OCR system. The size of the string should
    not change in length, as a lot of things are queried from the raw text, or the cleaned one.
    """
    input_text = (input_text.encode("ascii", "replace")).decode("utf-8")
    input_text = input_text.replace("?", " ")
    return (
        input_text.replace("-", " ")  # remove markup for some compound words
        .replace("\n", " ")  # remove newline
        .replace("\t", " ")  # remove tabs
    )


def convert_document_from_row_to_json_format(index_document, row_document):
    return {
        "document_id": index_document,
        "date": row_document.date.to_pydatetime().isoformat(),
        "title": {
            "raw": row_document["title"],
            "cleaned": _remove_ocr_markup_from_raw_text(row_document["title"]),
        },
        "text": {
            "raw": row_document["text"],
            "cleaned": _remove_ocr_markup_from_raw_text(row_document["text"]),
        },
        "cluster": row_document.get("cluster", None),
        "language": row_document.get("lang", None),
        "newspaper": {
            "title": row_document.get("source", "Unknown source"),
            "uri": [],
            "languages": [],
            "inception": None,
            "end": None,
        },
    }


def __extract_sentences_and_tokens_from_cleaned_text(events: list) -> list:
    list_of_events_analyzed = []
    list_of_keeped_entity = ["GPE", "ORG", "DATE", "PERSON"]
    for event in events:
        text_cleaned = language_processor(event["text"]["cleaned"])
        token_id = 0
        for sentence in text_cleaned.ents:
            tokens = list()
            for token in sentence:
                tokens.append(
                    {
                        "id": token_id,
                        "token": token.text,
                        "start_index": token.idx,
                        "end_index": token.idx + len(token),
                    }
                )
                token_id += 1
                event.setdefault("sentence", []).append(tokens)
                list_of_events_analyzed.append(event)
    return list_of_events_analyzed


def __extract_mentions_from_cleaned_text(events: list) -> list:
    list_of_events_analyzed = []
    entity_already_saved = []
    list_of_keeped_entity = ["GPE", "ORG", "DATE", "PER"]
    for event in events:
        text_cleaned = language_processor(event["text"]["cleaned"])
        token_id = 0
        for token in text_cleaned.ents:
            if token.text not in entity_already_saved:
                event.setdefault("mentions", []).append(
                    {
                        "id": token_id,
                        "type": token.label_,
                        "stance": token.sentiment,
                        "label": token.text,
                    }
                )
                entity_already_saved.append(token.text)
                token_id += 1
        list_of_events_analyzed.append(event)
    return list_of_events_analyzed


def create_folder(save_to: str):
    if not os.path.exists(f"{save_to}"):
        os.makedirs(f"{save_to}")


def main():
    parser = argparse.ArgumentParser(
        description="Convert a corpus in the document_tracking_resources package format to JSON."
    )
    parser.add_argument(
        "--corpus",
        type=str,
        required=True,
        help="Path to the corpus to convert to JSON",
    )
    parser.add_argument(
        "--output-file",
        type=str,
        required=True,
        help="Path to the output file where to store the JSON document",
    )
    parser.add_argument("--save-to", type=str, required=True)
    args = parser.parse_args()

    create_folder(args.save_to)
    documents_in_json_format = [
        convert_document_from_row_to_json_format(index, row)
        for index, row in read_pickle(args.corpus).iterrows()
    ]
    documents_in_json_format = __extract_mentions_from_cleaned_text(
        documents_in_json_format
    )
    with open(args.output_file, mode="w") as output_json_file:
        json.dump(documents_in_json_format, output_json_file, indent=2)


if __name__ == "__main__":
    main()
