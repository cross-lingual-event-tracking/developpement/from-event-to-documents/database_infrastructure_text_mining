#!/bin/bash

type_of_dataset="noise no_noise"
for type_dt in $type_of_dataset; do
repository="../data/pickle_data_concat_$type_dt"
    for index in ${repository}/*; do
        index_name_file=$(echo $index | tr "/" "\n" | tail -n 1)
        index_name=$(echo $index_name_file | tr "." "\n" | head -n 1)
        if [ $index_name == "miranda_original" ];
        then
            ./convert_document_tracking_resources_corpus_to_json.py --corpus ../data/pickle_data_concat_$type_dt/$index_name_file --output-file ../data/json_data/$type_dt/${index_name}_${type_dt}.json --save-to ../data/json_data/$type_dt
        fi
    done
done