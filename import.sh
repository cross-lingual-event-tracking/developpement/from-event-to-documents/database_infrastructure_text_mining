#!/bin/bash

type_of_dataset="noise no_noise"
for type_dt in $type_of_dataset; do
    ./import.py --data-repository ../data/json_data/$type_dt --clear-index True --password elastic
done